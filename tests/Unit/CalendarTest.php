<?php

namespace Tests\Unit;

use App\Services\CalendarService;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class CalendarTest extends TestCase
{
    /**
     * test get calendar when google API call is success
     */
    public function testGetCalendarSuccess(): void
    {
        $calendarService = new CalendarService();
        Http::fake([
            'googleapis.com/calendar/*' => Http::response([
                "kind"                 => "calendar#calendar", 
                "etag"                 => "", 
                "id"                   => "", 
                "summary"              => "", 
                "timeZone"             => "", 
                "conferenceProperties" => [
                      "allowedConferenceSolutionTypes" => [
                         "" 
                      ] 
                   ] 
             ], 200)
        ]);
        $response = $calendarService->getCalendar("access-token here", "1234");
        $this->assertNotEmpty($response);
        $this->assertEquals($response['code'], 200);
        $this->assertNotNull($response['data']);
    }
    /**
     * test get calendar when google API call is failed and errors array is present
     */
    public function testGetCalendarFailed1(): void
    {
        $calendarService = new CalendarService();
        Http::fake([
            'googleapis.com/calendar/*' => Http::response([
                "error" => [
                    "code"    => 401, 
                    "message" => "Request had invalid authentication credentials. Expected OAuth 2 access token, login cookie or other valid authentication credential. See https://developers.google.com/identity/sign-in/web/devconsole-project.", 
                    "errors"  => [
                        [
                            "message" => "Invalid Credentials", 
                            "domain" => "global", 
                            "reason" => "authError", 
                            "location" => "Authorization", 
                            "locationType" => "header" 
                        ] 
                    ], 
                    "status"  => "UNAUTHENTICATED" 
                ]
             ], 401)
        ]);
        $response = $calendarService->getCalendar("access-token here", "1234");
        $this->assertEquals($response['code'], 401);
        $this->assertNotNull($response['data']['errors']);
        $this->assertNotEmpty($response);
    }
    /**
     * test get calendar when google API call is failed and errors array is not present
     */
    public function testGetCalendarFailed2(): void
    {
        $calendarService = new CalendarService();
        Http::fake([
            'googleapis.com/calendar/*' => Http::response([
                "message" => "Error Message here", 
             ], 500)
        ]);
        $response = $calendarService->getCalendar("access-token here", "1234");
        $this->assertEquals($response['code'], 500);
        $this->assertNotNull($response['data']);
        $this->assertNotEmpty($response);
    }
    /**
     * test get calendar list when google API call is success
     */
    public function testGetCalendarListSuccess(): void
    {
        $calendarService = new CalendarService();
        Http::fake([
            'googleapis.com/calendar/*' => Http::response([
                "kind"          => "calendar#calendarList", 
                "etag"          => "", 
                "nextSyncToken" => "121313434234=", 
                "items"         => [
                      [
                         "kind" => "calendar#calendarListEntry", 
                         "etag" => "", 
                         "id" => "addressbook#contacts@group.v.calendar.google.com" 
                      ] 
                   ] 
             ], 200)
        ]);
        $response = $calendarService->getCalendarList("access-token here", "1234");
        $this->assertNotEmpty($response);
        $this->assertEquals($response['code'], 200);
        $this->assertNotNull($response['data']);
        $this->assertTrue(isset($response['data']['items']));
    }
}