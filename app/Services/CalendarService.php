<?php

namespace App\Services;

use App\Constants\CalendarConstants;
use App\Libraries\ApiResponseLibrary;
use Illuminate\Support\Facades\Http;

class CalendarService {    

    /**
     * getCalendar - Get a google calendar information (from all existing calendars globally)
     *
     * @param  string $accessToken - Google Access Token
     * @param  string $calendarId - Google Calendar Id
     * @return array
     */
    public function getCalendar(string $accessToken, string $calendarId) : array
    {
        $uri = CalendarConstants::CALENDAR_API_URI . env('GOOGLE_CALENDAR_VERSION') . CalendarConstants::CALENDAR_API_PATH . $calendarId;
        $curlResponse = Http::withHeaders([
            'Authorization' => 'Bearer ' . $accessToken
        ])->get($uri);
        
        return ApiResponseLibrary::formatResponse($curlResponse->status(), $curlResponse->json());
    }
        
    /**
     * getCalendarList - Get google calendar list of a user
     *
     * @param  string $accessToken - Google Access Token
     * @param  array $queryParams - optional query parameters
     * @return array
     */
    public function getCalendarList(string $accessToken, array $queryParams) : array
    {
        $uri = CalendarConstants::CALENDAR_API_URI . env('GOOGLE_CALENDAR_VERSION') ."/users/me". CalendarConstants::CALENDARLIST_API_PATH;
        $curlResponse = Http::withHeaders([
            'Authorization' => 'Bearer ' . $accessToken
        ])->get($uri, $queryParams);
        
        return ApiResponseLibrary::formatResponse($curlResponse->status(), $curlResponse->json());
    }
}