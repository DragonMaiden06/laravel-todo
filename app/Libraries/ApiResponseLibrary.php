<?php

namespace App\Libraries;

class ApiResponseLibrary {
        
    /**
     * formatResponse - format response of Google API
     *
     * @param  int $code
     * @param  array $data
     * @return array
     */
    public static function formatResponse(int $code, array $data) : array
    {
        if ($code !== 200) {
            return [
                "code" => $code,
                "data" => $data['error'] ?? $data
            ];
        }

        return [
            "code" => $code,
            "data" => $data
        ];
    }
}