<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;

class GetCalendarRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            "calendar_id" => 'required'
        ];
    }

    public function messages(): array
    {
        return [
            "calendar_id.required" => "calendar_id is required"
        ];
    }
}
