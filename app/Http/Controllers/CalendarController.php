<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetCalendarRequest;
use App\Services\CalendarService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    private $calendarService;
        
    /**
     * __construct
     *
     * @param  object $CalendarService
     * @return void
     */
    public function __construct(CalendarService $calendarService)
    {
        $this->calendarService = $calendarService;
    }
    
    /**
     * getCalendar - Get a google calendar information (from all existing calendars globally)
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function getCalendar(GetCalendarRequest $request) : JsonResponse
    {
        $response = $this->calendarService->getCalendar($request->header('Access-Token'), $request->calendar_id);
        return response()->json($response['data'], $response['code']);
    }
        
    /**
     * getCalendarList - Get google calendar list of a user
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function getCalendarList(Request $request) : JsonResponse
    {
        $response = $this->calendarService->getCalendarList($request->header('Access-Token'), $request->all());
        return response()->json($response['data'], $response['code']);
    }
}
