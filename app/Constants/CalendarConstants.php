<?php

namespace App\Constants;

class CalendarConstants {
    public const CALENDAR_API_URI = "https://www.googleapis.com/calendar/";
    public const CALENDAR_API_PATH = "/calendars/";
    public const CALENDARLIST_API_PATH = "/calendarList/";
}