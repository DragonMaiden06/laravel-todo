<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CalendarController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::middleware('check.accesstoken')->controller(CalendarController::class)->group(function() {
    Route::prefix('calendars')->group(function () {
        Route::get('/', 'getCalendar');
    });

    Route::prefix('calendar-list')->group(function () {
        Route::get('/list', 'getCalendarList');
    });
});
