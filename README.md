<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## Calendar API
Microservice that communicates with Google Calendar API.
Currently included in app are:
- Get Calendar - Get a google calendar information (from all existing calendars globally).
- Get Calendar List - Get google calendar list of a user.

## How to setup and run locally
1. Clone repository locally
    ```
    git clone {HTTPS repo link here}
    ```
2. Install necessary vendors (in project root directory)
    ```
    composer install
    ```
3. Run Artisan Serve to run locally (in project root directory)
    ```
    php artisan serve
    ```
4. You can access the API endpoints in localhost:8000 (port 8000 is the default port)

## Code Coverage
- Code coverage and unit testing is applied on business logic which is in ```app/Services``` Folder
- Run test command to see coverage result
    ```
    php artisan test --coverage
    ```